#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>

int main (int argc, char** argv){
	if (argc < 3){
	printf("Ingrese sus dos rutas");
		return -1;
	}
	umask(0);
	int ru1, ru2;
	ru1=open(argv[1], O_RDONLY);
	if (ru1<0 ){
		perror("No se pudo leer");	
		return -1;	
	}
	umask(0);
	ru2=open(argv[2],O_WRONLY|O_CREAT|O_TRUNC,S_IRWXU|S_IRWXO|S_IRWXG);	
	if (ru2<0 ){
		perror("No se pudo escribir");
		return -1;	
	}
	char buf[1000];
	memset(buf,0,1000);
	while(read(ru1,&buf,sizeof(char))!=0){
		write(ru2,&buf,sizeof(char));
		memset(buf,0,1000);
	} 
	close(ru1);
	close(ru2);
	return 0;
}
